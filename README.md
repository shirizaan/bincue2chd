I wrote a quick batch script to automate the conversion of Sega CD bin/cue files
to the chd file format. There are three scripts currently (for ease of use):

**bin7z2chd.bat**

If your source is bin and 7z files, use this. It will unzip them, un-ecm them
then convert them to chd files.

**bincue2chd.bat**

If your source is just bin/cue files, use this.

**gdi2chd.bat**

If you've got some dreamcast roms, you can use this. I don't know the version
of chdman that I've got here, I just threw this in as a nicety. I know that
some Dreamcast emulators won't work with certain versions of the chd format. I
may fix this in the future but probably not as it's not something I currently
use.