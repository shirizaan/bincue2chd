@echo off
for /r %%i in (*.cue) do (
	7za e "%%~ni.7z"
	rename "Track 01.bin.ecm" "%%~ni.bin.ecm"
	unecm "%%~ni.bin.ecm"
	chdman createcd -i "%%i" -o "%%~ni.chd"
)
pause
